//SHOPLIST PAGE
var $grid = $('.grid').isotope({
    itemSelector: '.grid-item',
    masonry: {
        columnWidth: 40,
        fitWidth: true
        }

});

$('.filter button').on("click", function() {
    var value = $(this).attr('data-name');
    $grid.isotope({
        filter: value
    })
})
